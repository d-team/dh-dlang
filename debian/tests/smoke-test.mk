#!/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=-lto

include /usr/share/dh-dlang/dlang-flags.mk

D_EXTRA_FLAGS ?= -of=test_exe

ifeq ($(DC),gdc)
D_EXTRA_FLAGS = -o test_exe
endif

all:
	$(DC) $(DFLAGS) $(D_EXTRA_FLAGS) debian/tests/test_exe.d
	./test_exe

.PHONY: all
