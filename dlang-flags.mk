#
# Set the DFLAGS global variable matching the current default D compiler on the given architecture,
# to apply the D flags we want for release builds in Debian.
# This also applies arch-specific quirks, like not building with NEON on armhf when LDC is used.
#
# This script is supposed to be included by debian/rules directly
#

include /usr/share/dpkg/buildflags.mk

DC = gdc

DC_LDC_FOUND := $(shell dpkg-query -W -f='$${Status}' ldc 2>/dev/null | grep -c "ok installed")
DC_GDC_FOUND := $(shell dpkg-query -W -f='$${Status}' gdc 2>/dev/null | grep -c "ok installed")

# by default, use GCC/GDC Debian optimization flags for D,
# but remove the warning flags that aren't supported in D.
DFLAGS = $(filter-out -O2 -O3 -Wformat -Werror=implicit-function-declaration -Wno-error=deprecated -Werror=format-security,$(CFLAGS)) -frelease
# only set LDC build flags if our compiler is LDC
ifeq ($(DC_LDC_FOUND),1)
    # translate GCC optimization flags to LDC flags, and add release and -wi flags
    DFLAGS_TRANSLATED = $(shell python3 /usr/share/dh-dlang/gcc-to-ldc-flags.py --dh-dlang-ignore-unknown-options $(CFLAGS))
    DC = ldc2
    DFLAGS = -release -wi $(DFLAGS_TRANSLATED)
    ifeq ($(DEB_HOST_ARCH),armhf)
        DFLAGS += -mattr=-neon
    endif
endif
export DFLAGS
export DC
