#
# Unfortunately, neither DMD nor LDC follow the GCC command-line syntax, which occasionally causes issues
# when projects use raw Makefiles and/or do not do any special treatment of externally added linker flags.
# Since LDC/DMD use the GCC linker, those flags are valid but need to be transformed into a form to have
# them passed through LDC to the linker.
#
# The small helper Python script shipped with dh-dlang achieves this task, and allows linker hardening flags
# used in Debian to be applied to D binaries.
#

# only set LDC-compatible flags if our compiler is LDC
ifeq ($(DC),ldc2)
    export LDFLAGS := $(shell python3 /usr/share/dh-dlang/gcc-to-ldc-flags.py $(shell dpkg-buildflags --get LDFLAGS))
endif
